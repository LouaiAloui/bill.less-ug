

# Description

NestJs + mongodb backend using passport.js and authGuard + authJWT to identification and authorisation . 
I take a look at the documentation that there is a powerfull way : ${'authentication guard as a global guard and instead of using @UseGuards() decorator on top of each controller'} . But in this exemple I don't need it . 

Completing the backend task with the bonus features .
The credential of the database and the jwt secret are in .env file which will be ignored during push to git . 
Do not worry I will send a capture of it via Email .


## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Aloui Louai](https://louaialoui.netlify.app)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

.
