import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = Userr & Document;

@Schema()
export class Userr {
  @Prop({ required: true })
  userId: number;

  @Prop()
  username?: string;

  @Prop()
  password?: string;

}

export const UserSchema = SchemaFactory.createForClass(Userr);