export class BaseTodoDto {
    title: string
    done?: boolean
}