  
import {  Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

// This class should return true when the "isPublic" metadata is found 
// for this we use Reflector class

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {}